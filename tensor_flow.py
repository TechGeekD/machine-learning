""" tensorflow Machine Learnig Fundas """
import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

# HELLO = tf.constant('Hello, TensorFlow!')
# SESS = tf.Session()
# print(SESS.run(HELLO))

# launch the model in an InteractiveSession
# sess = tf.InteractiveSession()


def weight_variable(shape):
        initial = tf.truncated_normal(shape, stddev=0.1)
        return tf.Variable(initial)

def bias_variable(shape):
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)

def conv2d(x, W):
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')

def max_pool_2x2(x):
    return tf.nn.max_pool(x, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')

def mnist_data():

        # input any number of MNIST images, each flattened into a 784-dimensional vector
    x = tf.placeholder(tf.float32, [None, 784])
    # y_ is the true distribution
    # To implement cross-entropy we need to first add a new placeholder to input the correct answers
    y_ = tf.placeholder(tf.float32, [None, 10])

    # weight: multiply the 784-dimensional image vectors by it to produce 10-dimensional
    # vectors of evidence for the difference classes
    W = tf.Variable(tf.zeros([784, 10]))
    # biases: shape of [10]
    b = tf.Variable(tf.zeros([10]))

    # to initialize the variables we created
    # sess.run(tf.global_variables_initializer())

    # multiply x by W with the expression tf.matmul then add b, and finally apply tf.nn.softmax
    y = tf.nn.softmax(tf.matmul(x, W) + b)

    # the cross-entropy is measuring how inefficient our predictions are for describing the truth
    # cross_entropy = tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(y), reduction_indices=[1]))
    # cross_entropy = tf.reduce_mean(
    #     tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y))

    # Train the Model: we will use steepest gradient descent, with a step length of 0.5, to descend the cross entropy.
    # train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    # Training the model can be accomplished by repeatedly running train_step.
    # for _ in range(1000):
        # Each step of the loop, we get a "batch" of one hundred random data
        # points from our training set
        # batch_xs, batch_ys = mnist.train.next_batch(100)
        # train_step feeding in the batches data to replace the placeholders
        # sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})

        # batch = mnist.train.next_batch(100)
        # train_step.run(feed_dict={x: batch[0], y_: batch[1]})

    # Evaluate the Model: How well did our model do?
    # tf.argmax(y,1) is the label our model thinks is most likely for each input
    # tf.argmax(y_,1) is the correct label
    # # tf.equal is to check if our prediction matches the truth
    # correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    # accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # we can evaluate our accuracy on the test data
    # print(sess.run(accuracy, feed_dict={x: mnist.test.images, y_: mnist.test.labels}))
    # print(accuracy.eval(
    #     feed_dict={x: mnist.test.images, y_: mnist.test.labels}))

    W_conv1 = weight_variable([5, 5, 1, 32])
    b_conv1 = bias_variable([32])
    x_image = tf.reshape(x, [-1, 28, 28, 1])

    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
    h_pool1 = max_pool_2x2(h_conv1)

    W_conv2 = weight_variable([5, 5, 32, 64])
    b_conv2 = bias_variable([64])

    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
    h_pool2 = max_pool_2x2(h_conv2)

    W_fc1 = weight_variable([7 * 7 * 64, 1024])
    b_fc1 = bias_variable([1024])

    h_pool2_flat = tf.reshape(h_pool2, [-1, 7 * 7 * 64])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)

    W_fc2 = weight_variable([1024, 10])
    b_fc2 = bias_variable([10])

    y_conv = tf.matmul(h_fc1_drop, W_fc2) + b_fc2

    cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
    train_step = tf.train.AdamOptimizer(1e-4).minimize(cross_entropy)
    correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for i in range(20000):
            batch = mnist.train.next_batch(50)
            if i % 100 == 0:
                train_accuracy = accuracy.eval(feed_dict={
                    x: batch[0], y_: batch[1], keep_prob: 1.0})
                print('step %d, training accuracy %g' % (i, train_accuracy))
            train_step.run(feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})

        print('test accuracy %g' % accuracy.eval(feed_dict={x: mnist.test.images, y_: mnist.test.labels, keep_prob: 1.0}))


mnist_data()

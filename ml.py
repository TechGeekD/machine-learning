#!/usr/bin/python

""" Basic Machine Learnig Fundas """
import numpy as np
from sklearn.datasets import load_iris
from sklearn import tree

IRIS = load_iris()

def decision_tree():
    # bumpy/orange 1/2 smooth/apple 0/3
    """ Return apple or orange based on training data. """
    features = [[140, 1], [130, 1], [150, 0], [170, 0]]
    labels = [2, 2, 3, 3]

    clf = tree.DecisionTreeClassifier()
    clf = clf.fit(features, labels)

    print(clf.predict([[120, 0]]))

def visualizing_decision_tree():
    """ Using SampleDataSet from sklearn """
    test_idx = [0, 50, 100]

    # training_data
    train_target = np.delete(IRIS.target, test_idx)
    train_data = np.delete(IRIS.data, test_idx, axis=0)

    # testing_data
    test_target = IRIS.target[test_idx]
    test_data = IRIS.data[test_idx]

    clf = tree.DecisionTreeClassifier()
    clf.fit(train_data, train_target)

    print(test_target)
    print(clf.predict(test_data))

    # viz code
    from sklearn.externals.six import StringIO
    import pydotplus

    dot_data = StringIO()
    tree.export_graphviz(clf,
                         out_file=dot_data,
                         feature_names=IRIS.feature_names,
                         class_names=IRIS.target_names,
                         filled=True,
                         rounded=True,
                         impurity=False)

    graph = pydotplus.graph_from_dot_data(dot_data.getvalue())
    graph.write_pdf("iris.pdf")

def train_test():
    """ Check accuracy of prediction """
    x = IRIS.data
    y = IRIS.target

    from sklearn.model_selection import train_test_split
    x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=.5)

    from sklearn.neighbors import KNeighborsClassifier
    my_classifire = KNeighborsClassifier()
    my_classifire.fit(x_train, y_train)

    prediction = my_classifire.predict(x_test)

    from sklearn.metrics import accuracy_score
    print(accuracy_score(y_test, prediction))

import random
from scipy.spatial import distance


def euc(a_node, b_node):
    """ Get distance from a to b """
    return distance.euclidean(a_node, b_node)

class ScrappyKNN():
    """ Custom Classifire """
    def __init__(self):
        self.x_train = None
        self.y_train = None

    def fit(self, x_train, y_train):
        """ Classifire fit """
        self.x_train = x_train
        self.y_train = y_train

    def predict(self, x_test):
        """ Classifire predict """
        prediction = []

        for row in x_test:
            label = self.closest(row)
            prediction.append(label)

        return prediction

    def closest(self, row):
        """ Classifire get closest node """
        best_dist = euc(row, self.x_train[0])
        best_index = 0

        for i in range(1, len(self.x_train)):

            dist = euc(row, self.x_train[i])

            if dist < best_dist:
                best_dist = dist
                best_index = i

        return self.y_train[best_index]


def own_classifire():
    """ Example of custom classifire """
    x_data = IRIS.data
    y_data = IRIS.target

    from sklearn.model_selection import train_test_split
    x_train, x_test, y_train, y_test = train_test_split(x_data, y_data, test_size=.5)

    # from sklearn.neighbors import KNeighborsClassifier
    my_classifire = ScrappyKNN()
    my_classifire.fit(x_train, y_train)

    prediction = my_classifire.predict(x_test)

    from sklearn.metrics import accuracy_score
    print(accuracy_score(y_test, prediction))


# decision_tree()
# visualizing_decision_tree()
# train_test()
own_classifire()
